default[:happystore][:database] = {
  :host => 'localhost',
  :username => 'happystore',
  :database => 'happystore',
  :password => 'happystore'
}

default[:benerator][:database] = {
  :host => 'localhost',
  :username => 'benerator',
  :database => 'benerator',
  :password => 'benerator'
}