#!/bin/sh
set -e

(echo 'n'; echo 'p'; echo; echo; echo; echo 'w') | fdisk /dev/sdb
# (echo 'n'; echo 'p'; echo; echo; echo; echo 'w') | fdisk /dev/sdc

mkfs -t ext4 /dev/sdb1
# mkfs -t ext4 /dev/sdc1

tune2fs -o journal_data_writeback /dev/sdb1

if [ `grep -c "/dev/sdb1" /etc/fstab` -eq 0 ]; then
	echo "/dev/sdb1/    /opt/fast_drive ext4    noatime,nodiratime,data=writeback,barrier=0,nobh,errors=remount-ro 0 3" >> /etc/fstab
	# echo "/dev/sdc1/    /opt/slow_drive ext4    defaults 0 4" >> /etc/fstab
fi

mkdir -p /opt/fast_drive /opt/slow_drive
mount /opt/fast_drive
# mount /opt/slow_drive
chown postgres /opt/fast_drive /opt/slow_drive

# Configure PostgreSQL server to accept connections from appserver
if [ `grep -c "192.168.56.0" /etc/postgresql/9.1/main/pg_hba.conf` -eq 0 ]; then
	echo "host	all		all		192.168.56.0/24		password" >> /etc/postgresql/9.1/main/pg_hba.conf
fi

# Configure PostgreSQL server to log long queries
if [ `grep -c ^log_min_duration_statement /etc/postgresql/9.1/main/postgresql.conf` -eq 0 ]; then
	echo "log_min_duration_statement = 700" >> /etc/postgresql/9.1/main/postgresql.conf
fi

/etc/init.d/postgresql restart

# Re-create DB with proper ENCODING CHARSET
sudo -u postgres psql -c "DROP DATABASE happystore;"
sudo -u postgres psql -c "CREATE DATABASE happystore WITH OWNER happystore TEMPLATE=template0 LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8' ENCODING ='UTF8' TABLESPACE=default CONNECTION LIMIT=-1;"

# Restore dump
sudo -u postgres psql happystore -f /vagrant/data/happystore_bck_201404011530

# Duplicate DB on fast and slow tablespaces in order to see improvement
sudo -u postgres psql -c "CREATE TABLESPACE fast_drive OWNER postgres LOCATION '/opt/fast_drive';"
sudo -u postgres psql -c "CREATE DATABASE happystore_fast WITH OWNER happystore TEMPLATE=happystore LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8' ENCODING ='UTF8' TABLESPACE=fast_drive CONNECTION LIMIT=-1;"
# sudo -u postgres psql -c"CREATE TABLESPACE slow_drive OWNER postgres LOCATION '/opt/slow_drive';\
# CREATE DATABASE happystore_slow WITH OWNER happystore TEMPLATE=happystore LC_COLLATE='en_US.UTF8' LC_CTYPE='en_US.UTF8' ENCODING ='UTF8' TABLESPACE=slow_drive CONNECTION LIMIT=-1;"

# Create a small subset of data for Stats refresh operation
# sudo -u postgres psql happystore -c "CREATE TABLE saleoperation_small AS (SELECT * FROM saleoperation LIMIT 5000);"
# sudo -u postgres psql -c "ALTER TABLE saleoperation_small ADD CONSTRAINT saleoperation_small_pkey PRIMARY KEY (id);"
# sudo -u postgres psql -c "VACUUM FULL saleoperation_small ;"

# Create a full subset of data for Stats refresh operation
sudo -u postgres psql happystore -c "CREATE TABLE saleoperation_full AS TABLE saleoperation;"
sudo -u postgres psql happystore -c "ALTER TABLE saleoperation_full ADD CONSTRAINT saleoperation_full_pkey PRIMARY KEY (id);"
sudo -u postgres psql happystore -c "VACUUM FULL saleoperation_full ;"

# Recreate table saleoperation in order to get bad stats
# sudo -u postgres psql happystore -c "DROP TABLE saleoperation;\
# CREATE TABLE saleoperation AS TABLE saleoperation_small;\
# ALTER TABLE saleoperation ADD CONSTRAINT saleoperation_pkey PRIMARY KEY (id);\
# ALTER TABLE saleoperation ADD FOREIGN KEY (product_id) REFERENCES product(id);\
# ALTER TABLE saleoperation ADD FOREIGN KEY (saletransaction_id) REFERENCES saletransaction(id);\
# DELETE FROM saleoperation;"

# INSERT INTO saleoperation (SELECT * from saleoperation_full);"
# ALTER TABLE saleoperation_full RENAME TO saleoperation;


# Reset stats in order to have bad performances
# sudo -u postgres psql happystore -c"SELECT  pg_stat_reset();"
