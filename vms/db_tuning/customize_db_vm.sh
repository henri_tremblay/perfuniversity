#!/bin/sh

myDB_VM_name=`vboxmanage list vms | awk '$1 ~/vms_db/ {print $1}' | sed 's/"//g'`

if [ `vboxmanage showvminfo ${myDB_VM_name} | grep State | grep -c running` -eq 1 ];then
	echo "VM ${myDB_VM_name} should be stopped before applying the custom setup…"
	exit 1
fi

if [ ${myDB_VM_name} ]; then
	# vboxmanage createhd --filename ./slow_drive.vdi --size 6144 --format VDI --variant standard
	vboxmanage createhd --filename ./fast_drive.vdi --size 6144 --format VDI --variant standard
	# vboxmanage bandwidthctl ${myDB_VM_name} add PerfUniversity_limited_bandwidth --type disk --limit 50M
	# vboxmanage storageattach ${myDB_VM_name} --storagectl "SATA Controller" --port 2 --type hdd --medium slow_drive.vdi --mtype normal --bandwidthgroup PerfUniversity_limited_bandwidth
	vboxmanage storageattach ${myDB_VM_name} --storagectl "SATA Controller" --port 1 --type hdd --medium fast_drive.vdi --mtype normal
fi