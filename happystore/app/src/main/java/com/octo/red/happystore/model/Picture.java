package com.octo.red.happystore.model;

import java.io.Serializable;

/**
 * Created by Henri on 13/04/2014.
 */
public class Picture implements Serializable {

    private byte[] data = new byte[65532];

    public byte[] getData() {
        return data;
    }
}
