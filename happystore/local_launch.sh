BASEDIR=$(dirname $0)
mvn -f ${BASEDIR}/app/pom.xml tomcat7:run $*
#Add these options to the command line when you have metrics up and running
#-Dmetrics.graphite.enabled=false -Dmetrics.graphite.port=8888 -Dmetrics.graphite.host=localhost