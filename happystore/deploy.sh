BASEDIR=$(dirname $0)
SERVER=appserver

ssh $SERVER "sudo service happystore stop"
scp $BASEDIR/app/target/happystore-1.0-SNAPSHOT.war $SERVER:happystore.war
ssh $SERVER "sudo chown tomcat:root happystore.war && sudo mv happystore.war /opt/tomcat/instances/happystore/webapps/"
ssh $SERVER "sudo service happystore start"
