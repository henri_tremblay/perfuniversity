#!/bin/sh

# Required when master-cap is installed locally
export DEV=1

# DB server
cap dbserver-devoxx chef:install -s user=vagrant
cap dbserver-devoxx chef:generate_local_json
cap dbserver-devoxx chef

# App server
cap appserver-devoxx chef:install -s user=vagrant
cap appserver-devoxx chef:generate_local_json
cap appserver-devoxx chef

# Tool server
cap toolserver-devoxx chef:install -s user=vagrant
cap toolserver-devoxx chef:generate_local_json
cap toolserver-devoxx chef
